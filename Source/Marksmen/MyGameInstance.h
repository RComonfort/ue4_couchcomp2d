// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MARKSMEN_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	UFUNCTION (BlueprintCallable)
	APlayerController * CreatePlayerController (int32 ControllerId, FString & OutError, bool bSpawnActor);

	UFUNCTION(BlueprintCallable)
	static bool WasPrimitiveComponentRenderedRecently (UPrimitiveComponent* _PrimitiveComponent, float _Tolerance);
	
};
