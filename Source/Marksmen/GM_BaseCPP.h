// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Types.h"
#include "GM_BaseCPP.generated.h"

/**
 * 
 */

UCLASS()
class MARKSMEN_API AGM_BaseCPP : public AGameModeBase
{
	GENERATED_BODY()
	
protected:
	UFUNCTION (BlueprintCallable, Category = "Base GM")
	bool FindSpawnPointFor (ETeams team, bool bSpawnLeader, const TArray<AActor*>& playerStarts, int32 inIndex, FVector& outResult);
};
