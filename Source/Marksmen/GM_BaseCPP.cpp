// Fill out your copyright notice in the Description page of Project Settings.

#include "GM_BaseCPP.h"
#include "PlayerStartCPP.h"
#include "Types.h"

bool AGM_BaseCPP::FindSpawnPointFor (ETeams team, bool bSpawnLeader, const TArray<AActor*>& playerStarts, int32 inIndex, FVector& outResult)
{
	int& i = inIndex;

	//UE_LOG (LogTemp, Warning, TEXT ("Looking at %d for team %d (leader?: %d)"), i, (uint8)team, bSpawnLeader);

	if (i < 0 || i >= playerStarts.Num ())
		return false;

	APlayerStartCPP* startActor = Cast<APlayerStartCPP> (playerStarts[i]);

	//UE_LOG (LogTemp, Warning, TEXT ("%s"), *startActor->GetName());

	if ((startActor->OwningTeam == ETeams::Neutral || startActor->OwningTeam == team) && startActor->bIsLeaderSpawn == bSpawnLeader)
	{
		outResult = startActor->GetActorLocation ();
		//UE_LOG (LogTemp, Warning, TEXT ("Found at %d for team %d (leader?: %d)"), i, (uint8)team, bSpawnLeader);
		return true;
	}
	else
		return FindSpawnPointFor (team, bSpawnLeader, playerStarts, i + (team == ETeams::Blue ? 1 : -1), outResult);
		

}