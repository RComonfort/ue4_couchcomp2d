// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Types.generated.h"


//Enum to identify teams
UENUM (BlueprintType)
enum class ETeams : uint8
{
	Red,
	Blue,
	Neutral,
};

//Enum for the type of spawn policy
UENUM (BlueprintType)
enum class ESpawnPolicy : uint8
{
	ES_FullSpawn, //All playing characters are to be placed in world
	ES_Recover, //If more than 2 players, recover fallen ally for last-battle winning team.
	ES_Respawn, //Respawn losing team to battle again
};
