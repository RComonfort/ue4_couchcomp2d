// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerStart.h"

#include "PlayerStartCPP.generated.h"

enum class ETeams :uint8;

/**
 * 
 */
UCLASS()
class MARKSMEN_API APlayerStartCPP : public APlayerStart
{
	GENERATED_BODY ()

public:
	UPROPERTY (BlueprintReadOnly, EditAnywhere, Category = "Player Start")
	int32 RoomIndex;

	UPROPERTY (BlueprintReadOnly, EditAnywhere, Category = "Player Start")
	ETeams OwningTeam;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Player Start")
	bool bIsLeaderSpawn;
};
